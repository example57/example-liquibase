package com.example.exampleliqubase.controller;

import java.security.Principal;

import com.example.exampleliqubase.api.AuthenticationService;
import com.example.exampleliqubase.dto.LoginRequestDTO;
import com.example.exampleliqubase.dto.RegistrationRequestDTO;
import com.example.exampleliqubase.model.Account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class AccountController {

    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping("/authorize")
    public Authentication authorize(@RequestBody @Validated LoginRequestDTO loginRequestDTO) {
        return authenticationService.authorize(loginRequestDTO);
    }

    @PostMapping("/register")
    public Account register(@RequestBody @Validated RegistrationRequestDTO registrationRequestDTO) {
        return authenticationService.register(registrationRequestDTO);
    }

    @PostMapping("/logout")
    public void logout() {
        authenticationService.logout();
    }

    @PostMapping("/current")
    public Principal current(Principal principal) {
        return principal;
    }
}
