package com.example.exampleliqubase.controller;

import java.util.List;

import com.example.exampleliqubase.api.EmploymentService;
import com.example.exampleliqubase.dto.EmploymentDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/employments")
public class EmploymentController {

    @Autowired
    private EmploymentService employmentService;

    @PostMapping("/save")
    public void saveEmployment(@RequestBody @Validated EmploymentDTO employmentDTO) {
        employmentService.saveEmployment(employmentDTO);
    }

    @PostMapping(value = "/records/update/{personId}")
    public void updateRecords(@PathVariable Long personId,
                              @RequestBody @Validated List<EmploymentDTO> employmentsDTO) {
        employmentService.updateRecords(employmentsDTO, personId);
    }

}
