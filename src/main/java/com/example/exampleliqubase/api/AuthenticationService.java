package com.example.exampleliqubase.api;

import com.example.exampleliqubase.dto.LoginRequestDTO;
import com.example.exampleliqubase.dto.RegistrationRequestDTO;
import com.example.exampleliqubase.model.Account;

import org.springframework.security.core.Authentication;

public interface AuthenticationService {
    Authentication authorize(LoginRequestDTO loginRequestDTO);

    Account register(RegistrationRequestDTO registrationRequestDTO);

    void logout();

}
