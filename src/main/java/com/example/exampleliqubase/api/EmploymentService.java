package com.example.exampleliqubase.api;

import java.util.List;

import com.example.exampleliqubase.dto.EmploymentDTO;

public interface EmploymentService {
    void saveEmployment(EmploymentDTO employmentDTO);

    void updateRecords(List<EmploymentDTO> employmentDTO, Long personId);
}
