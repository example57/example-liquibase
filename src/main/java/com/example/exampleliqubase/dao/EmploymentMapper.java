package com.example.exampleliqubase.dao;

import java.util.List;
import java.util.Set;

import com.example.exampleliqubase.model.EmploymentEntity;

import org.apache.ibatis.annotations.Param;

public interface EmploymentMapper {
    int deleteByPrimaryKey(Long employmentId);

    int insert(EmploymentEntity record);

    int insertSelective(EmploymentEntity record);

    EmploymentEntity selectByPrimaryKey(Long employmentId);

    int updateByPrimaryKeySelective(EmploymentEntity record);

    int updateByPrimaryKey(EmploymentEntity record);

    List<EmploymentEntity> getByPersonId(Long personId);

    void insertAll(@Param("list") List<EmploymentEntity> employmentsToSave);

    void updateAll(@Param("list") List<EmploymentEntity> employmentsToUpdate);

    void deleteByIds(@Param("ids") Set<Long> ids);
}