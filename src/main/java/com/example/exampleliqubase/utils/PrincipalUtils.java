package com.example.exampleliqubase.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;

import com.example.exampleliqubase.model.Account;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PrincipalUtils {

    public static User getUserDetailsFromAccount(Account account, String login, List<GrantedAuthority> authorities) {
        return new User(account.getUsername(), account.getPassword(), authorities);
    }
}
