package com.example.exampleliqubase.model;

import java.time.LocalDate;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
    * Инфорамция о человеке
    */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonEntity {
    /**
    * идентификатор.
    */
    private Long personId;

    /**
    * Имя
    */
    private String firstName;

    /**
    * Фамилия
    */
    private String lastName;

    /**
    * Фамилия
    */
    private String middleName;

    /**
    * Дата рождения
    */
    private LocalDate birthDate;

    /**
    * Пол
    */
    private String gender;
}