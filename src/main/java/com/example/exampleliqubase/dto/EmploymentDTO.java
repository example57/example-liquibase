package com.example.exampleliqubase.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Date;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmploymentDTO {
    /**
     * идентификатор.
     */
    private Long employmentId;

    /**
     * реализация оптимистической блокировки
     */
    @NotNull
    private Integer versionnnnn;

    /**
     * дата начала трудовой деятельности
     */
    private LocalDate startDt;

    /**
     * дата окончания трудовой деятельности
     */
    private LocalDate endDt;

    /**
     * тип деятельности
     */
    private Long workTypeId;

    /**
     * наименование организации.
     */
    private String organizationName;

    /**
     * адрес организации.
     */
    private String organizationAddress;

    /**
     * должность.
     */
    private String positionName;

    private Long personId;

}
