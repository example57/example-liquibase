package com.example.exampleliqubase.service;

import com.example.exampleliqubase.api.AuthenticationService;
import com.example.exampleliqubase.dao.AccountMapper;
import com.example.exampleliqubase.dto.LoginRequestDTO;
import com.example.exampleliqubase.dto.RegistrationRequestDTO;
import com.example.exampleliqubase.model.Account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    private AccountMapper accountMapper;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    @Transactional
    public Authentication authorize(LoginRequestDTO loginRequestDTO) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(loginRequestDTO.getLogin(), loginRequestDTO.getPassword());

        Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return authentication;
    }

    @Override
    public Account register(RegistrationRequestDTO registrationRequestDTO) {
        accountMapper.getByLogin(registrationRequestDTO.getUsername())
                .ifPresent((account)-> {
                    throw new RuntimeException("Such user already exist");
                });

        return createAccount(registrationRequestDTO);
    }

    @Override
    public void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    private Account createAccount(RegistrationRequestDTO registrationRequestDTO) {
        Account account = new Account();
        account.setPassword(passwordEncoder.encode(registrationRequestDTO.getPassword()));
        account.setUsername(registrationRequestDTO.getUsername());
        accountMapper.insert(account);
        return account;
    }
}
