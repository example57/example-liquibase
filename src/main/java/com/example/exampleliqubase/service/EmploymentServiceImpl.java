package com.example.exampleliqubase.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.example.exampleliqubase.api.EmploymentService;
import com.example.exampleliqubase.dao.EmploymentMapper;
import com.example.exampleliqubase.dto.EmploymentDTO;
import com.example.exampleliqubase.model.EmploymentEntity;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
public class EmploymentServiceImpl implements EmploymentService {

    @Autowired
    private EmploymentMapper employmentMapper;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void saveEmployment(EmploymentDTO employmentDTO) {
        EmploymentEntity employmentEntity = modelMapper.map(employmentDTO, EmploymentEntity.class);
        employmentMapper.insert(employmentEntity);
    }

    @Override
    public void updateRecords(List<EmploymentDTO> employmentsDTO, Long personId) {
        List<EmploymentEntity> employmentEntities = employmentMapper.getByPersonId(personId);

        Map<Long, EmploymentEntity> employmentEntityMap = employmentEntities.stream()
                .collect(Collectors.toMap(EmploymentEntity::getEmploymentId, Function.identity()));

        List<EmploymentEntity> employmentsToSave = new ArrayList<>();
        List<EmploymentEntity> employmentsToUpdate = new ArrayList<>();

        employmentsDTO.forEach(employmentDTO -> {
            EmploymentEntity newEmploymentEntity = modelMapper.map(employmentDTO, EmploymentEntity.class);
            Long employmentId = employmentDTO.getEmploymentId();

            if (employmentId == null) {
                employmentsToSave.add(newEmploymentEntity);
            } else {
                if (employmentEntityMap.containsKey(employmentId)) {
                    EmploymentEntity oldEmploymentEntity = employmentEntityMap.get(employmentId);

                    if (!newEmploymentEntity.equals(oldEmploymentEntity)) {
                        employmentsToUpdate.add(newEmploymentEntity);
                    }
                    employmentEntityMap.remove(employmentId);
                }
            }
        });

        if (!employmentEntityMap.isEmpty()) {
            employmentMapper.deleteByIds(employmentEntityMap.keySet());
        }

        if (!CollectionUtils.isEmpty(employmentsToSave)) {
            employmentMapper.insertAll(employmentsToSave);
        }

        if (!CollectionUtils.isEmpty(employmentsToUpdate)) {
            employmentMapper.updateAll(employmentsToUpdate);
        }
    }
}
